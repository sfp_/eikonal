#ifndef __COMMON_DEFS_HPP__
#define __COMMON_DEFS_HPP__

constexpr double pi = 3.141592653589793;
constexpr double sqrt2 = 1.414213562373095;
constexpr double sqrt3 = 1.732050807568877;
constexpr double sqrt6 = 2.449489742783178;

#endif // __COMMON_DEFS_HPP__
