import sys

sys.path.insert(0, '../build/Release')

import common3d
import eikonal as eik
import matplotlib.pyplot as plt
import numpy as np
import speedfuncs3d

from mpl_toolkits.mplot3d import Axes3D
from skimage import measure

if __name__ == '__main__':
    n = 2**4 + 1
    s = speedfuncs3d.s0
    l = np.linspace(-1, 1, n)
    Marcher = eik.Olim26Mid1
    m = Marcher(s(*np.meshgrid(l, l, l)), 2/(n - 1))
    i = int((n - 1)/2)
    m.addBoundaryNode(i, i, i)
    m.run()

    U = np.array(m)
    verts, faces, normals, values = measure.marching_cubes(U, 1)
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_trisurf(verts[:, 0], verts[:, 1], faces, verts[:, 2])
    plt.show()
